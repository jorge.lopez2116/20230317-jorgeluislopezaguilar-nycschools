//
//  SchoolTableViewCellTest.swift
//  nycschoolsTests
//
//  Created by Jorge Luis Lopez Aguilar on 20/01/23.
//

import XCTest
@testable import nycschools
class SchoolTableViewCellTest: XCTestCase {

    var svc: SchoolViewController!
    var testTable = UITableView()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
//        testTable.register(SchoolTableViewCell.self, forCellReuseIdentifier: "SchoolTableViewCell")
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        self.svc = storyboard.instantiateViewController(withIdentifier: "SchoolViewController") as? SchoolViewController
//        self.svc.loadViewIfNeeded()
        // in view controller, menuItems = ["one", "two", "three"]
//        self.svc.loadView()
        self.svc = SchoolViewController()
        self.svc.viewDidLoad()
    }
    
    override func setUpWithError() throws {
        svc = SchoolViewController()
        svc.loadViewIfNeeded()
    }

    override func tearDownWithError() throws {
        svc = nil
    }
    
//    func testCustomCell() {
//        let customCell: SchoolTableViewCell = testTable.dequeueReusableCell(withIdentifier: "SchoolTableViewCell") as! SchoolTableViewCell
//        XCTAssertNotNil(customCell, "No Custom School Cell Available")
//    }
    
    func testHasATableView() {
        XCTAssertNotNil(svc.schoolTableView)
    }
        
//    func testTableViewHasDelegate() {
//        XCTAssertNotNil(svc.schoolTableView.delegate)
//    }
        
//        func testTableViewConfromsToTableViewDelegateProtocol() {
//            XCTAssertTrue(viewControllerUnderTest.conforms(to: UITableViewDelegate.self))
//            XCTAssertTrue(viewControllerUnderTest.responds(to: #selector(viewControllerUnderTest.tableView(_:didSelectRowAt:))))
//        }
//
//        func testTableViewHasDataSource() {
//            XCTAssertNotNil(viewControllerUnderTest.tableView.dataSource)
//        }
//
//        func testTableViewConformsToTableViewDataSourceProtocol() {
//            XCTAssertTrue(viewControllerUnderTest.conforms(to: UITableViewDataSource.self))
//            XCTAssertTrue(viewControllerUnderTest.responds(to: #selector(viewControllerUnderTest.numberOfSections(in:))))
//            XCTAssertTrue(viewControllerUnderTest.responds(to: #selector(viewControllerUnderTest.tableView(_:numberOfRowsInSection:))))
//            XCTAssertTrue(viewControllerUnderTest.responds(to: #selector(viewControllerUnderTest.tableView(_:cellForRowAt:))))
//        }
//
//        func testTableViewCellHasReuseIdentifier() {
//            let cell = viewControllerUnderTest.tableView(viewControllerUnderTest.tableView, cellForRowAt: IndexPath(row: 0, section: 0)) as? SideMenuTableViewCell
//            let actualReuseIdentifer = cell?.reuseIdentifier
//            let expectedReuseIdentifier = "SideMenuTableViewCell"
//            XCTAssertEqual(actualReuseIdentifer, expectedReuseIdentifier)
//        }

}
