//
//  ViewController.swift
//  20230120-jorgeluislopezaguilar-nycschools
//
//  Created by Jorge Luis Lopez Aguilar on 20/01/23.
//

import UIKit

class SchoolViewController: UIViewController {
    
    @IBOutlet weak var schoolTableView: UITableView!
    private var schoolViewModel : SchoolsViewModel!
    private var dataSource : SchoolsTableViewDataSource<SchoolTableViewCell,Schools>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        callToViewModelForUIUpdate()
    }
    
    func callToViewModelForUIUpdate(){
        
        self.schoolViewModel =  SchoolsViewModel()
        self.schoolViewModel.bindSchoolViewModelToController = {
            self.updateDataSource()
        }
        
        if(self.schoolViewModel.callError){
            let alert = UIAlertController(title: "Error", message: "alert call", preferredStyle: .alert)

            let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
            })
            alert.addAction(ok)
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
        }
    }
    
    func updateDataSource(){
        self.dataSource = SchoolsTableViewDataSource(cellIdentifier: "SchoolTableViewCell", items: self.schoolViewModel.schData, configureCell: { (cell, evm) in
            cell.schoolDBNLabel.text = "DBN: \(evm.dbn)" // Concate Srings
            cell.schoolNameLabel.text = evm.schoolName
            cell.schoolLogo.loadFrom(URLAddress: "https://www.freeiconspng.com/thumbs/school-icon-png/school-icon-png-7.png")
        })
        DispatchQueue.main.async {
            if(self.schoolViewModel.callError){
                let alert = UIAlertController(title: "Error", message: "alert call", preferredStyle: .alert)

                let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
                })
                alert.addAction(ok)
                DispatchQueue.main.async(execute: {
                    self.present(alert, animated: true)
                })
            
            }
            self.schoolTableView.dataSource = self.dataSource
            self.schoolTableView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ShowDetail"{

            let destView = segue.destination as! SchoolDetailViewController
            let indexpath = self.schoolTableView.indexPathForSelectedRow
            let school = self.schoolViewModel.schData[indexpath!.row]
            destView.title = school.schoolName
//            print(school)
            destView.school = school
        }
        
    }
    
}
