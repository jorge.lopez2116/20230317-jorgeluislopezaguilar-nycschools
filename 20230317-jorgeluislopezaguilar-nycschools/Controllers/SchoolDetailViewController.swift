//
//  SchoolDetailViewController.swift
//  20230120-jorgeluislopezaguilar-nycschools
//
//  Created by Jorge Luis Lopez Aguilar on 20/01/23.
//

import UIKit
import MapKit
import CoreLocation

class SchoolDetailViewController: UIViewController, MKMapViewDelegate {
    @IBOutlet weak var satRecordsStack: UIStackView!
    
    // MARK: - School API Labels
    @IBOutlet weak var schoolDBNLabel: UILabel!
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var schoolOverviewLabel: UILabel!
    @IBOutlet weak var schoolCityLabel: UILabel!
    @IBOutlet weak var schoolNeighborhoodLabel: UILabel!
    
    // MARK: - SAT API Labels
    @IBOutlet weak var numOfSatTestTakersLabel: UILabel!
    @IBOutlet weak var satCriticalReadingAvgScoreLabel: UILabel!
    @IBOutlet weak var satMathAvgScoreLabel: UILabel!
    @IBOutlet weak var satWritingAvgScoreLabel: UILabel!
    @IBOutlet weak var noRecordLabel: UILabel!
    
    @IBOutlet weak var schoolMap: MKMapView!
    var locationManager: CLLocationManager?
    let maxHeighLabel: CGFloat = 20000
    
    private var satViewModel : SATViewModel!
    public var school : Schools!
    private var sharedFunctions : SharedFunctions!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.satRecordsStack.isHidden = true
        self.noRecordLabel.isHidden = true
        self.sharedFunctions =  SharedFunctions()
        
        callToViewModelForUIUpdate()
    }
    
    func callToViewModelForUIUpdate(){
        self.satViewModel =  SATViewModel()
        self.satViewModel.dbnSelected = school.dbn
        self.satViewModel.callFuncToGetSATData()
        self.satViewModel.bindSchoolViewModelToController = {
            self.updateDataSource()
        }
    }
    
    func updateDataSource(){
        
        DispatchQueue.main.async {
            self.schoolDBNLabel.text = self.school.dbn
            self.schoolNameLabel.text = self.school.schoolName
            self.schoolOverviewLabel.text = self.school.overviewParagraph
            self.schoolNeighborhoodLabel.text = self.school.neighborhood
            self.schoolCityLabel.text = self.school.city
            
            // Calculate Dinamically the number of lines for the Label
            self.schoolNameLabel.numberOfLines = self.sharedFunctions!.countLines(of: self.schoolNameLabel, maxHeight: self.maxHeighLabel)
            self.schoolOverviewLabel.numberOfLines = self.sharedFunctions!.countLines(of: self.schoolOverviewLabel, maxHeight: self.maxHeighLabel)

            // MARK: - Configure Map
            self.locationManager = CLLocationManager()
            self.locationManager?.requestWhenInUseAuthorization()
            self.schoolMap.mapType = .satellite
            self.schoolMap?.showsUserLocation = true

            
            let strLatitude = NSString.init(string: self.school.latitude ?? "0")
            let strLongitude = NSString.init(string: self.school.longitude ?? "0")
            
            if(strLatitude.doubleValue > 0){
                
                let coordinate = CLLocationCoordinate2D(latitude: strLatitude.doubleValue, longitude: strLongitude.doubleValue)
                let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
                let region = MKCoordinateRegion(center: coordinate, span: span)
                self.schoolMap.setRegion(region, animated: true)
                
                // Show artwork on map
                let pin = MKPointAnnotation()
                pin.coordinate = coordinate
                pin.title = self.school.schoolName
                self.schoolMap.addAnnotation(pin)
                
            }
                        
            if (self.satViewModel.schData != nil && self.satViewModel.schData.count > 0) {
                self.satRecordsStack.isHidden = false
                
                self.numOfSatTestTakersLabel.text = self.satViewModel.schData[0].numOfSatTestTakers
                self.satCriticalReadingAvgScoreLabel.text = self.satViewModel.schData[0].satCriticalReadingAvgScore
                self.satMathAvgScoreLabel.text = self.satViewModel.schData[0].satMathAvgScore
                self.satWritingAvgScoreLabel.text = self.satViewModel.schData[0].satWritingAvgScore
            }else{
                self.noRecordLabel.isHidden = false
            }
            
        }
        
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        mapView.centerCoordinate = userLocation.location!.coordinate
        mapView.mapType = .satellite
    }
    
}
