//
//  EmployeeTableViewCell.swift
//  20230120-jorgeluislopezaguilar-nycschools
//
//  Created by Jorge Luis Lopez Aguilar on 20/01/23.
//

import UIKit

// MARK: - loadFrom
// Function to Load one image assyncronus, in this case don't have
// School Logo in the JSON requested, but I added the function to show how can be function
extension UIImageView {
    func loadFrom(URLAddress: String) {
        if let url = URL(string: URLAddress) {
            URLSession.shared.dataTask(with: url) { (data, response, error) in
              // Error handling...
              guard let imageData = data else { return }

              DispatchQueue.main.async {
                self.image = UIImage(data: imageData)
              }
            }.resume()
          }
    }
}

class SchoolTableViewCell: UITableViewCell {
    
    @IBOutlet weak var schoolDBNLabel: UILabel!
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var schoolLogo: UIImageView!
    
    class var identifier: String { return String(describing: self) }
    class var nib: UINib { return UINib(nibName: identifier, bundle: nil) }
    
    private var sharedFunctions : SharedFunctions!
    let maxHeighLabel: CGFloat = 400
    
    var school : Schools? {
        didSet {
            schoolDBNLabel.text = "DBN: \(school?.dbn ?? "")"
            schoolNameLabel.text = school?.schoolName
            schoolLogo.loadFrom(URLAddress: "https://previews.123rf.com/images/anthonycz/anthonycz1710/anthonycz171000030/88338912-icono-de-la-escuela-.jpg")
            
            // Calculate Dinamically the number of lines for the Label
            schoolNameLabel.numberOfLines = self.sharedFunctions!.countLines(of: schoolNameLabel, maxHeight: self.maxHeighLabel)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initView() {
        // Cell view customization
        backgroundColor = .clear

        // Line separator full width
        preservesSuperviewLayoutMargins = false
        separatorInset = UIEdgeInsets.zero
        layoutMargins = UIEdgeInsets.zero
        self.sharedFunctions =  SharedFunctions()
        
        // Setup XCUITest Accessibility Labels
        schoolDBNLabel.accessibilityIdentifier = "label--schoolDBNLabel"
        schoolNameLabel.accessibilityIdentifier = "label--schoolNameLabel"
        
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        schoolDBNLabel.text = nil
        schoolNameLabel.text = nil
    }

}



    
    
    

    
