//
//  EmployeesViewModel.swift
//  20230120-jorgeluislopezaguilar-nycschools
//
//  Created by Jorge Luis Lopez Aguilar on 20/01/23.
//

import Foundation

class SATViewModel : NSObject {
    private var apiService : APIService!
    public var dbnSelected : String!
    //public var schoolSelected : Schools!
    
    private(set) var schData : [SAT]! {
        didSet {
            self.bindSchoolViewModelToController()
        }
    }
    
    var bindSchoolViewModelToController : (() -> ()) = {}
    
    override init() {
        super.init()
        self.apiService =  APIService()
//        callFuncToGetSATData()
    }
    
    func callFuncToGetSATData() {
        
        apiService.getSATData(dbn: self.dbnSelected) { success, model, error in
            if success {
//                print(model as Any)
                self.schData = model
            } else {
                print(error!)
            }
        }
        
    }
}
