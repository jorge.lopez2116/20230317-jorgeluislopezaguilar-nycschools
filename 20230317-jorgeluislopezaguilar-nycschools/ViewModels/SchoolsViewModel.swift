//
//  EmployeesViewModel.swift
//  20230120-jorgeluislopezaguilar-nycschools
//
//  Created by Jorge Luis Lopez Aguilar on 20/01/23.
//

import Foundation
import UIKit

class SchoolsViewModel : NSObject {
    private var apiService : APIService!
    public var callError : Bool = false
    private(set) var schData : [Schools]! {
        didSet {
            self.bindSchoolViewModelToController()
        }
    }
    
    var bindSchoolViewModelToController : (() -> ()) = {}
    
    override init() {
        super.init()
        self.apiService =  APIService()
        callFuncToGetEmpData()
    }
    
    func callFuncToGetEmpData() {

        apiService.getSchools { success, model, error in
            if success {
                self.schData = model
            } else {
                print(error!)
                self.callError = true
            }
        }

    }
}
