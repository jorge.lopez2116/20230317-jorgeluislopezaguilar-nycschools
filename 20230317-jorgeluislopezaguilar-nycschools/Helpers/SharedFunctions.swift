//
//  SharedFunctions.swift
//  20230120-jorgeluislopezaguilar-nycschools
//
//  Created by Jorge Luis Lopez Aguilar on 20/01/23.
//

import Foundation
import UIKit

class SharedFunctions :  NSObject {
    // Count Dinamically the number of lines for the Label, with Limit of Height
    func countLines(of label: UILabel, maxHeight: CGFloat) -> Int {
        guard let labelText = label.text else {
            return 0
        }
        print("** countLines: ", maxHeight)
        let rect = CGSize(width: label.bounds.width, height: maxHeight)
        let labelSize = labelText.boundingRect(with: rect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: label.font!], context: nil)
            
        let lines = Int(ceil(CGFloat(labelSize.height) / label.font.lineHeight))
        
        print("** countLines: ", lines)
        print("** countLines: ", lines)
        return labelText.contains("\n") && lines == 1 ? lines + 1 : lines
    }
}
