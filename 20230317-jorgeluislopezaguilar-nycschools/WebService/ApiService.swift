//
//  ApiService.swift
//  20230120-jorgeluislopezaguilar-nycschools
//
//  Created by Jorge Luis Lopez Aguilar on 20/01/23.
//

import Foundation

class APIService :  NSObject {
    
    private let sourcesURL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"

    func getSchools(completion: @escaping (Bool, [Schools]?, String?) -> ()) {
        HttpRequestHelper().GET(url: sourcesURL, params: ["$limit": "500"], httpHeader: .application_json) { success, data in
                if success {
                    do {
                        let model = try JSONDecoder().decode([Schools].self, from: data!)
                        completion(true, model, nil)
                    } catch {
                        print("Error: Trying to parse School to model")
                        completion(false, nil, "Error: Trying to parse School to model")
                    }
                } else {
                    print("Error: Schools GET Request failed")
                    completion(false, nil, "Error: School GET Request failed")
                }
            }
    }
    
    func getSATData(dbn: String, completion: @escaping (Bool, [SAT]?, String?) -> ()) {
        HttpRequestHelper().GET(url: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json", params: ["$limit": "500", "dbn":dbn], httpHeader: .application_json) { success, data in
                if success {
                    do {
                        let model = try JSONDecoder().decode([SAT].self, from: data!)
                        completion(true, model, nil)
                    } catch {
                        print("Error: Trying to parse SAT Data to model")
                        completion(false, nil, "Error: Trying to parse SAT Data to model")
                    }
                } else {
                    print("Error: SAT GET Request failed")
                    completion(false, nil, "Error: SAT Data GET Request failed")
                }
            }
    }

}

