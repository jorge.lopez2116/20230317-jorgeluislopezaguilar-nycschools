
//
//  School.swift
//  20230120-jorgeluislopezaguilar-nycschools
//
//  Created by Jorge Luis Lopez Aguilar on 20/01/23.
//

import Foundation

// MARK: - Schools
struct Schools: Decodable {
    let dbn: String
    let schoolName: String
    var overviewParagraph: String = ""
    var location: String = ""
    var latitude: String?
    var longitude: String?
    var neighborhood: String = ""
    var city: String = ""
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case overviewParagraph = "overview_paragraph"
        case location
        case latitude
        case longitude
        case neighborhood
        case city
    }

}
