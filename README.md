************************************************

# Results 

- Use Marks for comments
- Use Safe Areas
- Use Swift combined with objective-C (little part)
- Don't use Cocoapods

# Aditional 

- Used MVVM Patters
- Added Test Cases
- Added Dark/Light Mode 
- Added function to download and show an image(icon logo) in the tableview with Extensions
- Added extra info in the Detail View
- Added Map and Location Services in the Detail View
- Added Auto-Resize label in School Details

# Getting started

You'll need a working MacOS development environment with XCode 10 to use this
template. You can find instructions to get up and running on the Apple [XCode website](https://developer.apple.com/xcode/).

## What's contained in this project

# REQUIREMENTS:
These requirements are rather high-level and vague. If details are omitted, it is because we will be happy with any of a wide variety of solutions. Don't worry about finding "the" solution. Feel free to be creative with the requirements. Your goal is to impress (but do so with clean code).

Create a native app to provide information on NYC High schools.

Display a list of NYC High Schools. 
Get your data here: https://data.cityofnewyork.us/Education/DOE-High-School-Directory-2017/s3k6-pzi2
Selecting a school should show additional information about the school 
Display all the SAT scores - include Math, Reading and Writing. 
SAT data here: https://data.cityofnewyork.us/Education/SAT-Results/f9bf-2cp4
It is up to you to decide what additional information to display
When creating a name for your project, please use the following naming convention:
YYYYMMDD-[First&LastName]-NYCSchools (Example: 20180101-DanielleBordner-NYCSchools)
In order to prevent you from running down rabbit holes that are less important to us, try to prioritize the following:

# What is Important
Proper function – requirements met.
Well-constructed, easy-to-follow, commented code (especially comment hacks or workarounds made in the interest of expediency (i.e. // given more time I would prefer to wrap this in a blah blah blah pattern blah blah )).
Proper separation of concerns and best-practice coding patterns.
Defensive code that graciously handles unexpected edge cases.

# What is Less Important
Demonstrating technologies or techniques you are not already familiar with.
Bonus Points!
Unit Tests
Additional functionality – whatever you see fit.

# iOS:
 “For applications that include CocoaPods with their project code, please commit the third-party frameworks to your repository (Even though this goes against the CocoaPods general rules).”  OR don’t use any third-party dependency if possible, that will allow app to build and run standalone.
    - Be sure to use safe area insets.
    - Make sure your app is compatible with iPhone X.
    - Use Swift as the primary language. Combination of Swift and Objective would be good to show case your skills in both languages


# ScreenShots of the App:
![Image 1](http://www.newkontrol.com/jorge/img1.png)
![Image 2](http://www.newkontrol.com/jorge/img2.png)
![Image 3](http://www.newkontrol.com/jorge/img3.png)
![Image 4](http://www.newkontrol.com/jorge/img4.png)
![Image 5](http://www.newkontrol.com/jorge/img5.png)
![Image 6](http://www.newkontrol.com/jorge/img6.png)
![Image 7](http://www.newkontrol.com/jorge/img7.png)
![Image 8](http://www.newkontrol.com/jorge/img8.png)
![Image 9](http://www.newkontrol.com/jorge/img9.png)
![Image 10](http://www.newkontrol.com/jorge/img10.png)
